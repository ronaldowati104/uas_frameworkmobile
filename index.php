<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Project UAS Framework Mobile</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
	<link rel="icon" href="images/logo.png" type="image/x-icon">
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</head>
<body >
	
	<div id="root">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="#"><img src="images/logo.JPEG" alt="vue.js logo" class="logo-custom"> &nbsp IKATAN REMAJA GATAK</a>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="nav-link" href="">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<button class="btn btn-info" @click="showingaddModal = true;">Tambah Anggota</button>
				</li>
			</ul>
		</nav>
<br>
		<div class="container p-5 bg-light">
			<div class="row">

				<div class="alert alert-danger col-md-6" id="alertMessage" role="alert" v-if="errorMessage">
					{{ errorMessage }}
				</div>

				<div class="alert alert-success col-md-6" id="alertMessage" role="alert" v-if="successMessage">
					{{ successMessage }}
				</div>

<br>
				<table class="table table-striped">
					<thead class="thead bg-info text-white">
						<tr>
							<th>Id</th>
							<th>Nama</th>
							<th>Email</th>
							<th>Nomer HP</th>
							<th>Edit</th>
							<th>Hapus</th>
						</tr>
					</thead>
					<tbody class="tbody-custom">
						<tr v-for="uas1 in uas">
							<td>{{uas1.id}}</td>
							<td>{{uas1.nama}}</td>
							<td>{{uas1.email}}</td>
							<td>{{uas1.nope}}</td>
							<td><button @click="showingeditModal = true; selectUas(uas1);" class="btn btn-warning">Edit</button></td>
							<td><button @click="showingdeleteModal = true; selectUas(uas1);" class="btn btn-danger">Hapus</button></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="modal col-md-6" id="addmodal" v-if="showingaddModal">
				<div class="modal-head bg-light text-dark">
					<p class="p-left p-2">Tambah Anggota</p>
					<hr/>

					<div class="modal-body">
							<div class="col-md-12">
								<label for="nama">Nama</label>
								<input type="text" id="nama" class="form-control" v-model="newUas.nama" placeholder="Masukkan Nama">

								<label for="email">Email</label>
								<input type="text" id="email" class="form-control" v-model="newUas.email" placeholder="Masukkan Email">

								<label for="nope">Nomer HP</label>
								<input type="text" id="nope" class="form-control" v-model="newUas.nope" placeholder="Masukkan Nomer HP">
							</div>

						<hr/>
							<button type="button" class="btn btn-success"  @click="showingaddModal = false; addUas();">Simpan Perubahan</button>
							<button type="button" class="btn btn-danger"   @click="showingaddModal = false;">Tutup</button>
					</div>
				</div>
			</div>

		<div class="modal col-md-6" id="editmodal" v-if="showingeditModal">
			<div class="modal-head bg-light text-dark">
				<p class="p-left p-2">Edit Anggota</p>
				<hr/>

				<div class="modal-body">
						<div class="col-md-12">
							<label for="nama">Nama</label>
							<input type="text" id="nama" class="form-control" v-model="clickedUas.nama" placeholder="Masukkan Nama">

							<label for="email">Email</label>
							<input type="text" id="email" class="form-control" v-model="clickedUas.email" placeholder="Masukkan Email">

							<label for="nope">Nomer HP</label>
							<input type="text" id="nope" class="form-control" v-model="clickedUas.nope" placeholder="Masukkan Nomer HP">
						</div>

					<hr/>
						<button type="button" class="btn btn-success"  @click="showingeditModal = false; updateUas();">Simpan Perubahan</button>
						<button type="button" class="btn btn-danger"   @click="showingeditModal = false;">Tutup</button>
				</div>
			</div>
		</div>

		<div class="modal col-md-4 bg-info" id="deletemodal" v-if="showingdeleteModal">
			<div class="modal-head bg-light text-dark">
				<p class="p-left p-2">Hapus Anggota</p>
				<hr/>

				<div class="modal-body">
						<center>
							<p>Apakah yakin ingin hapus Anggota?</p>
							<h3>{{clickedUas.nama}}</h3>
					<hr/>
						<button type="button" class="btn btn-danger"  @click="showingdeleteModal = false; deleteUas();">Hapus</button>
						<button type="button" class="btn btn-warning"   @click="showingdeleteModal = false;">Batal</button>
						</center>
				</div>
			</div>
		</div>
	</div>

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
	<script src="js/app.js"></script>
</body>
</html>