var app = new Vue({

  el: "#root",
  data: {
  	showingaddModal: false,
  	showingeditModal: false,
  	showingdeleteModal: false,
  	errorMessage: "",
  	successMessage: "",
  	uas: [],
  	newUas: {nama: "", email: "", nope: ""},
  	clickedUas: {},
  },

  mounted: function () {
  	console.log("Vue.js is running...");
  	this.getAllUass();
  },

  methods: {
  	getAllUass: function () {
  		axios.get('http://localhost/uasvue/api/api.php?action=read')
  		.then(function (response) {
  			console.log(response);

  			if (response.data.error) {
  				app.errorMessage = response.data.message;
  			} else {
  				app.uas = response.data.uas;
  			}
  		})
  	},

  	addUas: function () {
  		var formData = app.toFormData(app.newUas);
  		axios.post('http://localhost/uasvue/api/api.php?action=create', formData)
  		.then(function (response) {
  			console.log(response);
  			app.newUas = {nama: "", email: "", nope: ""};

  			if (response.data.error) {
  				app.errorMessage = response.data.message;
  			} else {
  				app.successMessage = response.data.message;
  				app.getAllUass();
  			}
  		});
  	},

  	updateUas: function () {
  		var formData = app.toFormData(app.clickedUas);
  		axios.post('http://localhost/uasvue/api/api.php?action=update', formData)
  		.then(function (response) {
  			console.log(response);
  			app.clickedUas = {};

  			if (response.data.error) {
  				app.errorMessage = response.data.message;
  			} else {
  				app.successMessage = response.data.message;
  				app.getAllUass();
  			}
  		});
  	},

  	deleteUas: function () {
  		var formData = app.toFormData(app.clickedUas);
  		axios.post('http://localhost/uasvue/api/api.php?action=delete', formData)
  		.then(function (response) {
  			console.log(response);
  			app.clickedUas = {};

  			if (response.data.error) {
  				app.errorMessage = response.data.message;
  			} else {
  				app.successMessage = response.data.message;
  				app.getAllUass();
  			}
  		})
  	},

  	selectUas(uas1) {
  		app.clickedUas = uas1;
  	},

  	toFormData: function (obj) {
  		var form_data = new FormData();
  		for (var key in obj) {
  			form_data.append(key, obj[key]);
  		}
  		return form_data;
  	},

  	clearMessage: function (argument) {
  		app.errorMessage   = "";
  		app.successMessage = "";
  	},


  }
});