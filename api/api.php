<?php

	header("Content-type: application/json");

	// Koneksi database
	$conn = new mysqli("localhost", "root", "", "irega");
	if ($conn->connect_error) {
		die("Koneksi database gagal!");
	}
	$res = array('error' => false);

	// Read data dari database 
	$action = 'read';

	if (isset($_GET['action'])) {
		$action = $_GET['action'];
	}

	if ($action == 'read') {
		$result = $conn->query("SELECT * FROM `uas`");
		$uas  = array();

		while ($row = $result->fetch_assoc()) {
			array_push($uas, $row);
		}
		$res['uas'] = $uas;
	}

	// Insert data ke dalam database 
	if ($action == 'create') {
		$nama = $_POST['nama'];
		$email    = $_POST['email'];
		$nope   = $_POST['nope'];

		$result = $conn->query("INSERT INTO `uas` (`nama`, `email`, `nope`) VALUES('$nama', '$email', '$nope')");

		if ($result) {
			$res['message'] = "Berhasil Menambahkan Anggota";
		} else {
			$res['error']   = true;
			$res['message'] = "Gagal Menambahkan Anggota";
		}
	}

	// Update data
	if ($action == 'update') {
		$id   = $_POST['id'];
		$nama = $_POST['nama'];
		$email    = $_POST['email'];
		$nope   = $_POST['nope'];


		$result = $conn->query("UPDATE `uas` SET `nama`='$nama', `email`='$email', `nope`='$nope' WHERE `id`='$id'");

		if ($result) {
			$res['message'] = "Berhasil Update Anggota";
		} else {
			$res['error']   = true;
			$res['message'] = "Gagal update Anggota";
 		}
	}

	// Delete data
	if ($action == 'delete') {
		$id       = $_POST['id'];
		$nama = $_POST['nama'];
		$email    = $_POST['email'];
		$nope   = $_POST['nope'];

		$result = $conn->query("DELETE FROM `uas` WHERE `id`='$id'");

		if ($result) {
			$res['message'] = "Berhasil Delete Anggota";
		} else {
			$res['error']   = true;
			$res['message'] = "Gagal Delete Anggota";
		}
	}

	// Tutup koneksi database
	$conn->close();
	
	echo json_encode($res);
	die();
?>